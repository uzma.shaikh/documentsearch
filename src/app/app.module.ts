
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms'; 
import { RootComponent } from './root/root.component';
import { RootService } from './root/root.service';
import { FormsModule } from '@angular/forms'; 

@NgModule({
  declarations: [
    RootComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [RootService],
  bootstrap: [RootComponent]
})
export class AppModule { }