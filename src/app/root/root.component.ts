import { Component, OnInit } from '@angular/core';
import { RootService } from './root.service';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})

export class RootComponent implements OnInit {
  
  public email :string;
  public data :string;

  constructor(private rootService : RootService) { } 

  ngOnInit() { }

  onClick(FormData){          // gives (FormData i.e. query) as response
    this.rootService.postAPIData({'Query': FormData}).subscribe((response)=>{
      console.log('response from post data is ', response);
    },(error)=>{
      console.log('error during post is ', error)
    })
  }

}