import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RootService {

  constructor(private http: HttpClient) { }

  getAPIData(){
    return this.http.get('https://jsonplaceholder.typicode.com/users')
  }
  postAPIData({'Query': search}){
    return this.http.post('http://localhost:3000/api/postData', {'Query' : search }) // posting query from user to node server
  }
}